#!/bin/bash

ENVIRONMENTS=("local" "test" "development" "staging" "production")

if [[ " ${ENVIRONMENTS[@]} " =~ " ${1} " ]]; then
  ENVIRONMENT=${1:-"local"}

  if [ $ENVIRONMENT == 'test' ]; then
    COMMAND=${2:-"test"}
  else
    COMMAND=${2:-"up"}
  fi

  COMMAND_ARGS=${@:3}
else
  ENVIRONMENT="local"
  COMMAND=${1:-"up"}
  COMMAND_ARGS=${@:2}
fi

CAPITALIZED_ENVIRONMENT="$(tr '[:lower:]' '[:upper:]' <<<"${ENVIRONMENT:0:1}")${ENVIRONMENT:1}"

FILES='-f docker-compose.yml'
ENV_VAR="RAILS_ENV=$ENVIRONMENT "

echo ENVIRONMENT: $ENVIRONMENT
echo CAPITALIZED_ENVIRONMENT: $CAPITALIZED_ENVIRONMENT
echo COMMAND: $COMMAND
echo COMMAND_ARGS: $COMMAND_ARGS

if [ "$ENVIRONMENT" == "local" ] || [ $ENVIRONMENT == "test" ]; then
  FILES="$FILES -f docker-compose.local.yml"
fi

if [ $ENVIRONMENT == 'test' ]; then
  FILES="$FILES -f docker-compose.test.yml"
fi

if [ $ENVIRONMENT != 'local' ] && [ $ENVIRONMENT != 'test' ]; then
  DATABASE_URL=`aws ssm get-parameter --name /$CAPITALIZED_ENVIRONMENT/DATABASE_URL --with-decryption | jq -r '.Parameter.Value'`
  ENV_VAR="$ENV_VAR DATABASE_URL=$DATABASE_URL"

  SENTRY_DSN=`aws ssm get-parameter --name /$CAPITALIZED_ENVIRONMENT/SENTRY_DSN --with-decryption | jq -r '.Parameter.Value'`
  ENV_VAR="$ENV_VAR SENTRY_DSN=$SENTRY_DSN"
fi

echo DATABASE_URL: $DATABASE_URL
echo FILES: $FILES
echo ENV_VAR: $ENV_VAR

if [ "$COMMAND" == 'lint' ] || [ "$COMMAND" == 'rubocop' ]; then
  COMMAND="run api rubocop"
elif [ "$COMMAND" == 'test' ] || [ "$COMMAND" == 'rspec' ]; then
  COMMAND="run api rspec"
elif [ "$COMMAND" == 'bundle' ]; then
  COMMAND="run api bundle"
elif [ "$COMMAND" == 'migrate' ]; then
  COMMAND="run api rake db:create db:migrate"
elif [ "$COMMAND" == 'swagger' ]; then
  COMMAND="run api rake rswag:specs:swaggerize"
elif [ "$COMMAND" == 'console' ]; then
  COMMAND="run api rails console"
else
  COMMAND=$COMMAND
fi

FULL_COMMAND="$ENV_VAR docker-compose $FILES $COMMAND $COMMAND_ARGS; docker-compose $FILES down"
FULL_COMMAND="$ENV_VAR docker-compose $FILES $COMMAND $COMMAND_ARGS"

echo FULL_COMMAND: $FULL_COMMAND
eval $FULL_COMMAND
declare -i RETURN_CODE=$?

function cleanup {
  docker-compose $FILES down
  exit $RETURN_CODE
}

trap cleanup EXIT

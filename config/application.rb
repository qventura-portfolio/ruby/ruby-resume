require_relative 'boot'

require 'rails'

require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'

Bundler.require(*Rails.groups)

# rubocop:disable Style/ClassAndModuleChildren
module RubyResume
  class Application < Rails::Application
    config.api_only = true
    config.i18n.available_locales = [:en, :fr]
    config.load_defaults 5.2
    config.filter_parameters << :password
  end
end
# rubocop:enable Style/ClassAndModuleChildren

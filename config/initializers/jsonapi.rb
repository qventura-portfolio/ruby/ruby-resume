JSONAPI::Rails.configure do |config|
  config.jsonapi_object = nil
end

class JSONAPI::Rails::LogSubscriber < ActiveSupport::LogSubscriber
  def logger
    Rails.logger
  end
end

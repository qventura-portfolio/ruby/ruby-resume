Mobility.configure do |config|
  config.default_backend = :jsonb
  config.default_options[:locale_accessors] = true
  config.default_options[:fallbacks] = { fr: :en }
end

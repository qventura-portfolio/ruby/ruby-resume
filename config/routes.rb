Rails.application.routes.draw do
  if !Rails.env.production?
    mount Rswag::Api::Engine, at: '/docs'
    mount Rswag::Ui::Engine, at: '/docs'
  end

  namespace :admin_api, path: 'admin-api' do
    namespace :v1 do
      resources 'companies', only: [:create, :update]
      resources 'profiles', only: [:create]
      get 'profile', to: 'profiles#show'
      put 'profile', to: 'profiles#update'
    end
  end

  namespace :api do
    namespace :v1 do
      resources 'profiles', only: [:show]
    end
  end
end

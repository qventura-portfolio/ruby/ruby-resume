require 'support/test_helper'

RSpec.configure do |config|
  config.swagger_root = Rails.root.to_s + '/swagger'

  config.swagger_docs = {
    'admin-api/v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'AdminApi V1 Docs',
        version: 'v1',
      },
      paths: {

      },
    },
    'api/v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'AdminApi V1 Docs',
        version: 'v1',
      },
      paths: {

      },
    },
  }
end

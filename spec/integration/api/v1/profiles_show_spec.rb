require 'support/integration_helper'

describe 'Api::V1::Profiles',
  type: :request,
  swagger_doc: 'api/v1/swagger.json' do
  path '/api/v1/profiles/{id}' do
    get 'Show a Profile' do
      tags 'Profile'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'Public profile is shown' do
        let(:profile) { create(:profile) }
        let(:id) { profile.id }

        run_test! do |response|
          response_attributes = JSON.parse(response.body)['data']['attributes']
            .with_indifferent_access

          response_attributes.each do |key, value|
            expect(value).to eq(profile.send(key))
          end
        end
      end
    end
  end
end

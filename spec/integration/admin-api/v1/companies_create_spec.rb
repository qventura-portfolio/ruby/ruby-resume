require 'support/integration_helper'

describe 'AdminApi::V1::Companies',
  type: :request,
  swagger_doc: 'admin-api/v1/swagger.json' do
  path '/admin-api/v1/companies' do
    post 'Creates a Company' do
      tags 'Company'
      consumes 'application/json'
      parameter name: :company, in: :body, schema: {
        type: :object,
        properties: {
          data: {
            type: :object,
            properties: {
              attributes: {
                type: :object,
                properties: {
                  name_translations: {
                    type: :object,
                    properties: {
                      en: { type: :string },
                      fr: { type: :string },
                    },
                  },
                },
              },
            },
          },
        },
      }

      response '201', 'Company is created' do
        attributes = {
          name_translations: {
            en: "Night\'s Watch",
            fr: 'Garde de Nuit',
          },
        }.with_indifferent_access

        let(:company) do
          {
            data: {
              attributes: attributes,
            },
          }
        end

        run_test! do |response|
          response_attributes = JSON
            .parse(response.body)['data']['attributes']
            .with_indifferent_access

          attributes.each do |key, value|
            expect(response_attributes.with_indifferent_access[key])
              .to eq(value)
          end
        end
      end
    end
  end
end

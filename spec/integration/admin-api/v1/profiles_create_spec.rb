require 'support/integration_helper'

describe 'AdminApi::V1::Profiles',
  type: :request,
  swagger_doc: 'admin-api/v1/swagger.json' do
  path '/admin-api/v1/profiles' do
    post 'Creates a Profile' do
      tags 'Profile'
      consumes 'application/json'
      parameter name: :profile, in: :body, schema: {
        type: :object,
        properties: {
          data: {
            type: :object,
            properties: {
              attributes: {
                type: :object,
                properties: {
                  first_name: { type: :string },
                  last_name: { type: :string },
                  title_translations: {
                    type: :object,
                    properties: {
                      en: { type: :string },
                      fr: { type: :string },
                    },
                  },
                },
              },
            },
          },
        },
      }

      response '201', 'Profile is created' do
        attributes = {
          first_name: 'Jon',
          last_name: 'Snow',
          title_translations: {
            en: "Lord commander of the Night\'s Watch",
            fr: 'Lord commandant de la Garde de Nuit',
          },
        }.with_indifferent_access

        let(:profile) do
          {
            data: {
              attributes: attributes,
            },
          }
        end

        run_test! do |response|
          response_attributes = JSON
            .parse(response.body)['data']['attributes']
            .with_indifferent_access

          attributes.each do |key, value|
            expect(response_attributes.with_indifferent_access[key])
              .to eq(value)
          end
        end
      end
    end
  end
end

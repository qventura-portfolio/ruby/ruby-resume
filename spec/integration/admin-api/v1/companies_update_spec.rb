require 'support/integration_helper'

describe 'AdminApi::V1::Companies',
  type: :request,
  swagger_doc: 'admin-api/v1/swagger.json' do
  path '/admin-api/v1/companies/{id}' do
    put 'Update an existing Company' do
      tags 'Company'
      consumes 'application/json'
      parameter name: :id, in: :path, type: :string
      parameter name: :company, in: :body, schema: {
        type: :object,
        properties: {
          data: {
            type: :object,
            properties: {
              attributes: {
                type: :object,
                properties: {
                  name_translations: {
                    type: :object,
                    properties: {
                      en: { type: :string },
                      fr: { type: :string },
                    },
                  },
                },
              },
            },
          },
        },
      }

      response '200', 'Profile is updated' do
        let!(:existing_company) { create(:company) }
        let(:id) { existing_company.id }

        attributes = {
          name_translations: {
            en: 'Iron Throne',
            fr: 'Trône de Fer',
          },
        }.with_indifferent_access

        let(:company) do
          {
            data: {
              attributes: attributes,
            },
          }
        end

        run_test! do |response|
          existing_company.assign_attributes(attributes)

          response_attributes = JSON.parse(response.body)['data']['attributes']
            .with_indifferent_access

          attributes.each do |key, value|
            expect(response_attributes.with_indifferent_access[key])
              .to eq(value)
          end
        end
      end
    end
  end
end

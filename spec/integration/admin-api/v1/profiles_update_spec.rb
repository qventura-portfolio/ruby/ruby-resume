require 'support/integration_helper'

describe 'AdminApi::V1::Profiles',
  type: :request,
  swagger_doc: 'admin-api/v1/swagger.json' do
  path '/admin-api/v1/profile' do
    put 'Update an existing Profile' do
      tags 'Profile'
      consumes 'application/json'
      parameter name: :profile, in: :body, schema: {
        type: :object,
        properties: {
          data: {
            type: :object,
            properties: {
              attributes: {
                type: :object,
                properties: {
                  first_name: { type: :string },
                  last_name: { type: :string },
                  title_translations: {
                    type: :object,
                    properties: {
                      en: { type: :string },
                      fr: { type: :string },
                    },
                  },
                },
              },
            },
          },
        },
      }

      response '200', 'Profile is updated' do
        let!(:existing_profile) { create(:profile) }

        attributes = {
          first_name: 'Aegon',
          last_name: 'Targaryen',
          title_translations: {
            en: 'Rightful Heir To The Iron Throne',
            fr: 'Héritier légitime du Trône de Fer',
          },
        }.with_indifferent_access

        let(:profile) do
          {
            data: {
              attributes: attributes,
            },
          }
        end

        run_test! do |response|
          existing_profile.assign_attributes(attributes)

          response_attributes = JSON.parse(response.body)['data']['attributes']
            .with_indifferent_access

          attributes.each do |key, value|
            expect(response_attributes.with_indifferent_access[key])
              .to eq(value)
          end
        end
      end
    end
  end
end

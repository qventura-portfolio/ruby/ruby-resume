FactoryBot.define do
  factory :company do
    profile { create(:profile) }
    name_translations do
      {
        en: "Night\'s Watch",
        fr: 'Garde de Nuit',
      }
    end
  end
end

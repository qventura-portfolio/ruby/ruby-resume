class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :user_id
      t.jsonb :title, default: {}

      t.timestamps
    end

    add_index :profiles, :user_id
  end
end

class CreateExperiences < ActiveRecord::Migration[5.2]
  def change
    create_table :experiences do |t|
      t.references :company
      t.jsonb :title
      t.text :description
      t.datetime :start_date
      t.datetime :end_time
      t.string :is_current
      t.string :boolean

      t.timestamps
    end
    add_index :experiences, :title
  end
end

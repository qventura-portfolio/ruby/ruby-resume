class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.references :profile
      t.jsonb :name
      t.string :logo
      t.string :city
      t.string :country

      t.timestamps
    end
    add_index :companies, :name
  end
end

FROM ruby:2.5-alpine

RUN apk add --update \
  build-base \
  tzdata \
  postgresql-dev \
  && rm -rf /var/cache/apk/*

WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

ENV PORT 8080
EXPOSE 8080

CMD ['rails', 'server', '-b', '0.0.0.0', '-p', '8080']

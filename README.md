# README

## Toolset

A toolset is available to simplify usage of the application within the docker containers.
It basically wraps the `docker-compose` command, and concatanate files depending of the environment.
For instamce, the test command `./scripts/toolset.sh test` will result in executing this command: `RAILS_ENV=test docker-compose -f docker-compose.yml -f docker-compose.local.yml -f docker-compose.test.yml run api rspec`

First argument is an environment.

If the first argument is not a predefined environment (local, development, test, staging, production), the default environment (local) will be used.

Arguments after the environment (if given, else, all arguments) will be used as a command. Some commands are alias, other are gonna be forwarded directly to the docker-compose commands.

## Environments

Local environment is using the local postgres database.

Development, Staging and Production environments are the remote environments. The toolset uses AWS SSM to get configuration (DATABASE_URL, SENTRY_DSN, etc.) for each environment.

## Commands

If no command is provided after the environment (even if no environment is provided, i.e., if the default one is used.), the command `up` will be executed, resulting in starting the stack.

`./scripts/toolset.sh local` or `./scripts/toolset.sh` is going to start the local environment

`./scripts/toolset.sh development` is going to start the development environment

### Bundle install:
`./scripts/toolset.sh bundle x` (x being any command we want to send to bundle, like install)

This will run `bundle install` (or any other command) in the `api` service

### Migrate:
`./scripts/toolset.sh migrate`

This will run `rake db:create db:migrate` in the `api` service

### Test:
`./scripts/toolset.sh test` or `./scripts/toolset.sh rspec`

This will run `rspec` in the `api` service

### Lint:
`./scripts/toolset.sh lint` or `./scripts/toolset.sh rubocop`

This will run `rubocop` in the `api` service

### Console:
`./scripts/toolset.sh console`

This will run `rails console` in the `api` service

### Swagger:
`./scripts/toolset.sh swagger`

This will run `rake rswag:specs:swaggerize` in the `api` service

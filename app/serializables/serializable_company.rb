class SerializableCompany < JSONAPI::Serializable::Resource
  type 'company'

  attributes :profile_id
end

class SerializableProfile < JSONAPI::Serializable::Resource
  type 'profile'

  attributes :first_name, :last_name
end

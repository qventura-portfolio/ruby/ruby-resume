class BaseService
  class_attribute :_base_model
  attr_accessor :profile

  def initialize(profile)
    @profile = profile
  end

  def self.base_model(value)
    self._base_model = value
  end

  def method_missing(method_name, *args, &block)
    if self._base_model
      self._base_model.send(method_name, *args, &block)
    else
      super
    end
  end

  def respond_to_missing?(method_name, *args)
    if self._base_model
      self._base_model.respond_to_missing?(method_name, *args)
    else
      super
    end
  end

  def update(query, attributes)
    record = self._base_model.find_by(query)
    record.assign_attributes(attributes)
    record.save

    record
  end
end

class ProfilesService < BaseService
  base_model Profile

  def find_current
    Profile.last
  end

  def update_current(params)
    profile = find_current
    profile.assign_attributes(params)
    profile.save!

    profile
  end
end

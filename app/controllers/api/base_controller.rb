class Api::BaseController < ApplicationController
  def jsonapi_class
    Hash.new { |h, k| h[k] = "Api::Serializable#{k}".safe_constantize }
  end
end

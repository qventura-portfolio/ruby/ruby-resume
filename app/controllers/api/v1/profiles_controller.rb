class Api::V1::ProfilesController < Api::V1::BaseController
  service_klass ProfilesService

  def show
    render jsonapi: ProfilesService.new({}).find(params[:id])
  end
end

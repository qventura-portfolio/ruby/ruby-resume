class ApplicationController < ActionController::API
  class_attribute :_service_klass

  def self.service_klass(klass)
    self._service_klass = klass
  end

  def service
    self._service_klass&.new(current_profile)
  end

  def whitelisted_attributes
    params
      .require(:data)
      .require(:attributes)
  end

  def current_profile
    @current_profile ||= ProfilesService.new(nil).find_current
  end
end

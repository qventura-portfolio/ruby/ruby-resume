class AdminApi::BaseController < ApplicationController
  def jsonapi_class
    Hash.new { |h, k| h[k] = "AdminApi::Serializable#{k}".safe_constantize }
  end
end

class AdminApi::V1::CompaniesController < AdminApi::V1::BaseController
  service_klass CompaniesService

  def show
    render jsonapi: CompaniesService.new({}).find(params[:id])
  end

  def create
    render jsonapi: CompaniesService.new({}).create(whitelisted_attributes),
      status: :created
  end

  def update
    render jsonapi: CompaniesService.new({}).update(
      { id: params[:id] },
      whitelisted_attributes
    )
  end

  def whitelisted_attributes
    super.permit(name_translations: {})
  end
end

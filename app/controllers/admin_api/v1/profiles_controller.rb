class AdminApi::V1::ProfilesController < AdminApi::V1::BaseController
  service_klass ProfilesService

  def show
    render jsonapi: ProfilesService.new({}).find_current
  end

  def create
    render jsonapi: ProfilesService.new({}).create(whitelisted_attributes),
      status: :created
  end

  def update
    render jsonapi: ProfilesService.new({}).update_current(whitelisted_attributes)
  end

  def whitelisted_attributes
    super.permit(:first_name, :last_name, title_translations: {})
  end
end

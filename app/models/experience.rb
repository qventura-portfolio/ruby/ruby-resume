class Experience < ApplicationRecord
  include Translatable

  belongs_to :company

  translates :name

  delegate :profile, to: :company
end

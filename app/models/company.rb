class Company < ApplicationRecord
  include Translatable

  belongs_to :profile
  has_many :experiences, dependent: :destroy

  translates :name
end

class Profile < ApplicationRecord
  include Translatable

  has_many :experiences, dependent: :destroy

  translates :title
end

module Translatable
  extend ActiveSupport::Concern

  included do
    extend Mobility

    # rubocop:disable Metrics/MethodLength
    def self.translates(*attributes, **options)
      attributes.each do |attribute|
        class_eval <<-METHOD_DEF, __FILE__, __LINE__ + 1
          def #{attribute}_translations=(values, **options)
            value_hash = values ? values.to_h.with_indifferent_access : {}

            value_hash.each do |locale, translations_value|
              writter_method = "#{attribute}_\#\{locale\}="

              self.send(writter_method, translations_value)
            end
          end

          def #{attribute}_translations
            read_attribute(:#{attribute})
          end
        METHOD_DEF
      end

      mobility_accessor(*attributes, **options)
    end
    # rubocop:enable Metrics/MethodLength
  end
end
